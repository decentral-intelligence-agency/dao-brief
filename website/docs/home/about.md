---
id: about
title: About
---

This is a brand new idea from a few of us on Friday, but the gist so far is that this Discord can be a place of 'collective intelligence' collection/fusion/distribution around DAOs and best practices. Hence the 'Decentral Intelligence Agency'.